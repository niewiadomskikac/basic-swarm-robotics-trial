# Used/planned Components
## Chassis
[Black Gladiator Tracked Robot Chassis](https://www.dfrobot.com/product-1860.html)
## Low-level chassis controller
[Arduino Uno Board](https://store.arduino.cc/products/arduino-uno-rev3/)
### DC motors controller
[Cytron Arduino Shield](https://www.robotshop.com/media/files/content/c/cyt/pdf/cytron-10a-7-30v-dual-channel-dc-motor-driver-shield-datasheet.pdf)
## High-level controller
[Raspberry PI; model to be checked](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/)
## Power management
### Used Batteries
[Sony 18650](https://www.powerstream.com/p/us18650vtc4.pdf)

- Those will be packed into a parallel connected basket
### Battery Charger
[XTAR VP4C Charger](https://www.xtar.cc/product/XTAR-VP4C-Charger-113.html)