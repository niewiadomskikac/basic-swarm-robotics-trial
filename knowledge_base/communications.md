# Arduino - Raspberry Pi communication
## By USB
[USB communication](https://www.woolseyworkshop.com/2020/02/05/controlling-an-arduino-from-a-raspberry-pi/) 

Example contains code sample that will be used to receive data from arduino Uno and to emit it from raspberry Pi
# Pi 2 Pi communication
## Non - hierarchical networks 
### IBSS- Ad - Hoc networking
[IBSS](http://etutorials.org/Networking/802.11+security.+wi-fi+protected+access+and+802.11i/Part+II+The+Design+of+Wi-Fi+Security/Chapter+13.+Wi-Fi+LAN+Coordination+ESS+and+IBSS/IBSS+Ad-Hoc+Networks/)
#### OLSR protocol
[OLSR](https://tldp.org/HOWTO/OLSR-IPv6-HOWTO/olsrlinux.html)
### Mesh networks
[Mesh](https://github.com/binnes/WiFiMeshRaspberryPi)
### MQTT network architecture
[MQTT on Raspberry Pi](https://www.emqx.com/en/blog/use-mqtt-with-raspberry-pi)
### Network card
[TP-Link TL-WN722N](https://www.ceneo.pl/4560208#)