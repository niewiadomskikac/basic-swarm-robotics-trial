# basic swarm robotics trial 

This project is supposed to end in creating a functional 2 or 3 robot swarm, with its exact task (or tasks) to be specified at a later date

## Project state

Currently the author is building a knowledgebase that will enable proper project design and implementation

## Project directiories

- Swarm_robotics &rarr; Arduino code directory created with PlatformIO

- knowledge_base &rarr; Here you will find usefuli information on outlined issues with highlighted sources on th beginning of .md documents

## Used tools

- Text editor &rarr; Visual Studio Code (Windows 10)

- Schematics &rarr; Fritzing 

- Arduino compilation and building will be handled by PlatformIO