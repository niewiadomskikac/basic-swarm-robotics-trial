
#include <input_management.h>
CytronMD motor1(PWM_DIR, 3, 4);  // PWM 1 = Pin 3, DIR 1 = Pin 4.
CytronMD motor2(PWM_DIR, 9, 10); // PWM 2 = Pin 9, DIR 2 = Pin 10.
bot_drive bot_object;
// The setup routine runs once when you press reset.
void setup()
{

  Serial.begin(9600);

  bot_object.test_pb_encoder();
  delay(1000);
}

// The loop routine runs over and over again forever.
void loop()
{
  bot_object.pb_decoder();
  Serial.println("Starting");
  Serial.print("Left engine speed : ");
  Serial.println(bot_object.left_engine_speed_getter());
  Serial.print("Right engine speed : ");
  Serial.println(bot_object.right_engine_speed_getter());
  motor1.setSpeed(bot_object.left_engine_speed_getter());  // Motor 1 runs forwards at 50% speed.
  motor2.setSpeed(bot_object.right_engine_speed_getter()); // Motor 2 runs backwards at 50% speed.
  delay(6000);
  /*
  Serial.println("Ending mode 1 (50\% speed)");
  Serial.println("Starting mode 2 (100\% speed)");
  motor1.setSpeed(255);  // Motor 1 runs forward at full speed.
  motor2.setSpeed(-255); // Motor 2 runs backward at full speed.
  delay(6000);
  Serial.println("Ending mode 2 (100\% speed)");
  Serial.println("Stopping");
  motor1.setSpeed(0); // Motor 1 stops.
  motor2.setSpeed(0); // Motor 2 stops.
  delay(6000);
  Serial.println("Starting");
  Serial.println("Starting mode 1 (50\% speed)");
  motor1.setSpeed(-128); // Motor 1 runs backward at 50% speed.
  motor2.setSpeed(128);  // Motor 2 runs forward at 50% speed.
  delay(6000);
  Serial.println("Ending mode 1 (50\% speed)");
  Serial.println("Starting mode 2 (100\% speed)");
  motor1.setSpeed(-255); // Motor 1 runs backward at full speed.
  motor2.setSpeed(255);  // Motor 2 runs forward at full speed.
  delay(6000);
  Serial.println("Ending mode 2 (100\% speed)");
  Serial.println("Stopping");
  motor1.setSpeed(0); // Motor 1 stops.
  motor2.setSpeed(0); // Motor 2 stops.
  delay(6000);
  */
}