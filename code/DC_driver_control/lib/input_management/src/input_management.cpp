#include "input_management.h"
void bot_drive::test_pb_encoder()
{
    movement_vector message = movement_vector_init_zero;
    pb_ostream_t stream = pb_ostream_from_buffer(buffer, sizeof(buffer));
    message.x_value = -20;
    message.y_value = 20;
    message.movement_state = movement_vector_Movement_state_STAND_STILL;
    status = pb_encode(&stream, movement_vector_fields, &message);
    message_length = stream.bytes_written;
    for (unsigned int i = 0; i < stream.bytes_written; i++)
    {
        Serial.print(buffer[i], HEX);
    }
};

bool bot_drive::pb_decoder()
{
    movement_vector decoded_message = movement_vector_init_zero;
    pb_istream_t decoded_stream = pb_istream_from_buffer(buffer, message_length);
    status = pb_decode(&decoded_stream, movement_vector_fields, &decoded_message);
    input_movement_vector = decoded_message;
    if (!status)
    {
        printf("Decoding failed: %s\n", PB_GET_ERROR(&decoded_stream));
        return 1;
    }
    Serial.print("x_value : ");
    Serial.println((int)decoded_message.x_value);
    Serial.print("y_value : ");
    Serial.println((int)decoded_message.y_value);
    Serial.println("movement_state : ");
    Serial.println((String)decoded_message.movement_state);
    protobuf_values_interpreter(decoded_message, engine_speeds);
    return 0;
}
void bot_drive::protobuf_values_interpreter(movement_vector used_vector, int8_t *engine_speeds_array)
{
    double atan_value = atan(used_vector.x_value / used_vector.y_value);
    atan_value = atan_value / abs(atan_value);
    direct_engine_state_function(static_cast<int16_t>(used_vector.movement_state), atan_value);
}
void bot_drive::direct_engine_state_function(int16_t movement_val, double turn_parameter)
{
    if (movement_val == 2)
        movement_val = -1;

    engine_speeds[0] = 100 * int(turn_parameter) + 70 * movement_val;
    engine_speeds[1] = -100 * int(turn_parameter) + 70 * movement_val;
}
bot_drive::bot_drive()
{
}
bot_drive::~bot_drive()
{
}