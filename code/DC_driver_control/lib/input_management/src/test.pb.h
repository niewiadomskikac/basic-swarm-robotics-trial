/* Automatically generated nanopb header */
/* Generated by nanopb-0.4.5 */

#ifndef PB_PROTO_FILES_TEST_PB_H_INCLUDED
#define PB_PROTO_FILES_TEST_PB_H_INCLUDED
#include <pb.h>

#if PB_PROTO_HEADER_VERSION != 40
#error Regenerate this file with the current version of nanopb generator.
#endif

/* Enum definitions */
typedef enum _movement_vector_Movement_state
{
    movement_vector_Movement_state_STAND_STILL = 0,
    movement_vector_Movement_state_FORWARDS = 1,
    movement_vector_Movement_state_BACKWARDS = 2
} movement_vector_Movement_state;

/* Struct definitions */
/* *
movement vector contains instructions for arduino microcontroller to execute */
typedef struct _movement_vector
{
    int64_t x_value; /* horizontal value (target horizontal value - current horizontal value) */
    int64_t y_value; /* distance from said object */
    movement_vector_Movement_state movement_state;
} movement_vector;

/* Helper constants for enums */
#define _movement_vector_Movement_state_MIN movement_vector_Movement_state_STAND_STILL
#define _movement_vector_Movement_state_MAX movement_vector_Movement_state_BACKWARDS
#define _movement_vector_Movement_state_ARRAYSIZE ((movement_vector_Movement_state)(movement_vector_Movement_state_BACKWARDS + 1))

#ifdef __cplusplus
extern "C"
{
#endif

/* Initializer values for message structs */
#define movement_vector_init_default              \
    {                                             \
        0, 0, _movement_vector_Movement_state_MIN \
    }
#define movement_vector_init_zero                 \
    {                                             \
        0, 0, _movement_vector_Movement_state_MIN \
    }

/* Field tags (for use in manual encoding/decoding) */
#define movement_vector_x_value_tag 1
#define movement_vector_y_value_tag 2
#define movement_vector_movement_state_tag 3

/* Struct field encoding specification for nanopb */
#define movement_vector_FIELDLIST(X, a)        \
    X(a, STATIC, SINGULAR, SINT64, x_value, 1) \
    X(a, STATIC, SINGULAR, SINT64, y_value, 2) \
    X(a, STATIC, SINGULAR, UENUM, movement_state, 3)
#define movement_vector_CALLBACK NULL
#define movement_vector_DEFAULT NULL

    extern const pb_msgdesc_t movement_vector_msg;

/* Defines for backwards compatibility with code written before nanopb-0.4.0 */
#define movement_vector_fields &movement_vector_msg

/* Maximum encoded size of messages (where known) */
#define movement_vector_size 24

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif
