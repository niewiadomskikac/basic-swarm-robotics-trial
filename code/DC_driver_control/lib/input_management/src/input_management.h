#include <MultiMap.h>
#include <pb_arduino.h>
#include <Arduino.h>
#include <math.h>
#include "CytronMotorDriver.h"
#include "pb_common.h"
#include "pb.h"
#include "pb_encode.h"
#include "test.pb.h"
/**
 * Class to encompass all control and protobuf related functionalities in this project.
 *
 */
class bot_drive
{
private:
    uint8_t buffer[128];
    size_t message_length;
    movement_vector input_movement_vector;
    bool status;
    /**
     * Array keeping values of robot engines
     * @param engine_speeds[0]: speed of left engine
     * @param engine_speeds[1]: speed of right engine
     */
    int8_t engine_speeds[2];
    /**
     * This function processes given x and y values into speeds and directions for engines when turning
     *
     * @param protobuf_values_array
     * @param speeds_and_directions_table
     */
    void protobuf_values_interpreter(movement_vector used_vector, int8_t *speeds_and_directions_array);
    /**
     * @brief This function will check whether to go forwards, backwards, or turn, based on protobuf values
     *
     * @param protobuf_values_array
     */
    void engine_movement_function(movement_vector used_vector, double angle_arctg);
    void direct_engine_state_function(int16_t val, double angle_val);

public:
    int8_t left_engine_speed_getter()
    {
        return engine_speeds[0];
    }
    int8_t right_engine_speed_getter()
    {
        return engine_speeds[1];
    }
    /**
     * This function decodes protobuf packages and
     *
     * @return int : expected
     */
    bool pb_decoder();
    /**
     * Function used solely to test protobuf decoding w/out the need to connect it to RPi, to be deleted in final version
     * @return returns 0 if no error,otherwise returns 1 and error message
     */
    void test_pb_encoder();
    /**
     * @brief Construct a new bot drive object
     *
     */
    bot_drive();
    /**
     * @brief Destroy the bot drive object
     *
     */
    ~bot_drive();
};
