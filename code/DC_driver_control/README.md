# Basics swarm robotics trial project

## Project state

Development may slow down due to masters thesi, but its active

## Aim

Aim of this project is

- To create at least 2 simple wheeled robots

  - Robots will have an Arduino Uno unit used for control of DC motors connected to wheels

  - Robots will have a Raspberry Pi unit that will make use of camera to pick point to drive to

- To implement some form of robot cooperation in software

  - Some ideas on examples of implemented cooperation could be:

    - Pushing a ball to a point

    - Mapping out a room
